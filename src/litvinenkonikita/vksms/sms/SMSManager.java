package litvinenkonikita.vksms.sms;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import litvinenkonikita.vksms.Const;
import litvinenkonikita.vksms.Utils;
import litvinenkonikita.vksms.ui.list.SmsDialogItem;
import litvinenkonikita.vksms.ui.list.SmsItem;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds;
import android.util.Log;

public class SMSManager {
	
	private static final String TAG = SMSManager.class.getSimpleName();
	
	private static final String[] SMS_COLUMNS = {Const.SMS_ID, Const.SMS_THREAD_ID, 
												Const.SMS_ADDRESS, Const.SMS_PERSON, 
												Const.SMS_DATE, Const.SMS_BODY, Const.SMS_TYPE};
	
	private static final String[] UNIQUE_ADDRESS_COLUMNS = {"DISTINCT " + Const.SMS_ADDRESS, Const.SMS_PERSON, 
															Const.SMS_DATE, Const.SMS_BODY, Const.SMS_TYPE};
	
	private Context mContext;
	
	public SMSManager(Context context){
		mContext = context;
	}
/**
	public ArrayList<Map<String, Object>> getSmsThreads(){
		String selection = Const.SMS_ADDRESS + " IS NOT NULL) GROUP BY (" + Const.SMS_ADDRESS;
		///String selection = null;
		String selectionArgs[] = null;
		String sortOrder = Const.SMS_ADDRESS;
		Uri smsUri = Uri.parse("content://sms");
		Cursor cursor = mContext.getContentResolver().query(smsUri, UNIQUE_ADDRESS_COLUMNS, selection, selectionArgs, sortOrder);
		ArrayList<Map<String, Object>> dialogsList = new ArrayList<Map<String, Object>>();
		long currentTime = System.currentTimeMillis();
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(currentTime);
		int currentYear = cal.get(Calendar.YEAR);
		if(cursor.getCount() > 0){
			SimpleDateFormat simpleDateFormat;
			while(cursor.moveToNext()){
				
				String address = cursor.getString(0);
				long contactId = cursor.getLong(1);
				long timestamp = cursor.getLong(2);
				String body = cursor.getString(3);
				long type = cursor.getLong(4);
				cal.setTimeInMillis(timestamp);
				int year = cal.get(Calendar.YEAR);
				ArrayList<String> contactNames = getContactName(address);

				if(year < currentYear){
					simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				}
				else{
					simpleDateFormat = new SimpleDateFormat("MM/dd");
				}
				Date date = new Date(timestamp);
				String dateTime = simpleDateFormat.format(date);
				
				Map<String, Object> listItem = new HashMap<String, Object>();
				listItem.put(Const.SMS_ADDRESS, address);
				///listItem.put(Const.SMS_PERSON, contactId);
				String contactName = null;
				if(contactNames.size() > 0){
					contactName = contactNames.get(0);
					listItem.put(Const.SMS_PERSON, contactName);
				}
				listItem.put(Const.SMS_DATE, dateTime);
				listItem.put(Const.SMS_BODY, body);
				listItem.put(Const.SMS_TYPE, type);
				dialogsList.add(listItem);
				
				Utils.Log.d(TAG, "address = " + address + 
							///" contactId = " + contactId + 
							" contactName = " + contactName + 
							" timestamp = " + timestamp + 
							" body = " + body + 
							" type = " + type);
			}
		}
		cursor.close();
		return dialogsList;
	}
*/	
	
	public ArrayList<SmsDialogItem> getSmsDialogs(){
		String selection = Const.SMS_ADDRESS + " IS NOT NULL AND (" + Const.SMS_TYPE + " = " + Const.INCOME_SMS + " OR " + 
																	Const.SMS_TYPE + " = " + Const.OUTCOME_SMS +")) GROUP BY (" + Const.SMS_ADDRESS;
		///String selection = null;
		String selectionArgs[] = null;
		String sortOrder = Const.SMS_ADDRESS;
		Uri smsUri = Uri.parse("content://sms");
		Cursor cursor = mContext.getContentResolver().query(smsUri, UNIQUE_ADDRESS_COLUMNS, selection, selectionArgs, sortOrder);
		ArrayList<SmsDialogItem> dialogsList = new ArrayList<SmsDialogItem>();
		long currentTime = System.currentTimeMillis();
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(currentTime);
		int currentYear = cal.get(Calendar.YEAR);
		if(cursor.getCount() > 0){
			SimpleDateFormat simpleDateFormat;
			while(cursor.moveToNext()){
				
				String address = cursor.getString(0);
				long contactId = cursor.getLong(1);
				long timestamp = cursor.getLong(2);
				String body = cursor.getString(3);
				long type = cursor.getLong(4);
				cal.setTimeInMillis(timestamp);
				int year = cal.get(Calendar.YEAR);
				ArrayList<String> contactNames = getContactName(address);

				if(year < currentYear){
					simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
				}
				else{
					simpleDateFormat = new SimpleDateFormat("MM/dd");
				}
				Date date = new Date(timestamp);
				String dateTime = simpleDateFormat.format(date);
				
				///Map<String, Object> listItem = new HashMap<String, Object>();
				///listItem.put(Const.SMS_ADDRESS, address);
				String contactName = null;
				if(contactNames.size() > 0){
					contactName = contactNames.get(0);
					///Utils.Log.w(TAG, "contactName = " + contactName);
					///listItem.put(Const.SMS_PERSON, contactName);
				}
				/**listItem.put(Const.SMS_DATE, dateTime);
				listItem.put(Const.SMS_BODY, body);
				listItem.put(Const.SMS_TYPE, type);*/
				
				SmsDialogItem listItem = new SmsDialogItem(contactName, address, body, dateTime);
				dialogsList.add(listItem);
				
				Utils.Log.d(TAG, "address = " + address + 
							///" contactId = " + contactId + 
							" contactName = " + contactName + 
							" timestamp = " + timestamp + 
							" body = " + body + 
							" type = " + type);
			}
		}
		cursor.close();
		return dialogsList;
	}
	
	
	public ArrayList<SmsItem> getSmsByAddress(String address){
		String selection = Const.SMS_ADDRESS + " =  ? AND (" + Const.SMS_TYPE + " = " + Const.INCOME_SMS + " OR " + Const.SMS_TYPE + " = " + Const.OUTCOME_SMS + ")";
		String selectionArgs[] = { address };
		String sortOrder = Const.SMS_DATE;
		Uri smsUri = Uri.parse("content://sms");
		Cursor cursor = mContext.getContentResolver().query(smsUri, SMS_COLUMNS, selection, selectionArgs, sortOrder);
		ArrayList<SmsItem> dialogsList = new ArrayList<SmsItem>();
		long currentTime = System.currentTimeMillis();
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(currentTime);
		int currentYear = cal.get(Calendar.YEAR);
		if(cursor.getCount() > 0){
			SimpleDateFormat simpleDateFormat;
			while(cursor.moveToNext()){
				//long messageId = cursor.getLong(0);
				//long threadId = cursor.getLong(1);
				///String address = cursor.getString(2);
				//long contactId = cursor.getLong(3);
				long timestamp = cursor.getLong(4);
				String body = cursor.getString(5);
				int type = cursor.getInt(6);
				
				cal.setTimeInMillis(timestamp);
				int year = cal.get(Calendar.YEAR);

				if(year < currentYear){
					simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd, hh:mm");
				}
				else{
					simpleDateFormat = new SimpleDateFormat("MM/dd, hh:mm");
				}
				Date date = new Date(timestamp);
				String dateTime = simpleDateFormat.format(date);
				
				SmsItem smsItem = new SmsItem(dateTime, body, type);

				dialogsList.add(smsItem);
				/**
				Utils.Log.d(TAG, "time = " + dateTime + 
							" body = " + body + 
							" type = " + type);*/
			}
		}
		cursor.close();// delete
		return dialogsList;
	}

	
	private ArrayList<String> getContactName(String number){
		ArrayList<String> phones = new ArrayList<String>();
		Cursor cursor = mContext.getContentResolver().query(
		        CommonDataKinds.Phone.CONTENT_URI, 
		        null, 
		        CommonDataKinds.Phone.NUMBER + " = ?", 
		        new String[]{ number }, null);
		
		while (cursor.moveToNext()){
		    phones.add(cursor.getString(cursor.getColumnIndex(CommonDataKinds.Phone.DISPLAY_NAME)));
		}
		
		return phones;
	}
}
