package litvinenkonikita.vksms.ui.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import litvinenkonikita.vksms.R;

public class DialogsListAdapter extends ArrayAdapter<SmsDialogItem> {

	private static final String TAG = DialogsListAdapter.class.getSimpleName();
	
	private int mResource;
	
	public DialogsListAdapter(Context context, int resource, List<SmsDialogItem> objects) {
		super(context, resource, objects);
		/**for(SmsDialogItem item : objects){
			Utils.Log.w(TAG, "ContactName: " + item.getContactName() + 
								" Number: " + item.getNumber() + 
								" Message: " + item.getMessage() + 
								" Date: " + item.getDate());
		}*/
		mResource = resource;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View v = convertView;
		
		if(v == null){
			LayoutInflater vi = LayoutInflater.from(getContext());
			v = vi.inflate(mResource, null);
		}
		
		SmsDialogItem dialog = getItem(position);
		
		if(dialog != null){
			TextView contactNameTextView = (TextView) v.findViewById(R.id.contactName);
			TextView contactPhoneTextView = (TextView) v.findViewById(R.id.contactPhone);
			TextView messageTextView = (TextView) v.findViewById(R.id.messageText);
			TextView smsTimeTextView = (TextView) v.findViewById(R.id.smsTime);
			
			if(contactPhoneTextView != null){
				contactPhoneTextView.setText(dialog.getNumber());
			}
			
			if(messageTextView != null){
				messageTextView.setText(dialog.getMessage());
			}
			
			if(smsTimeTextView != null){
				smsTimeTextView.setText(dialog.getDate());
			}
			
			if(contactNameTextView != null && dialog.getContactName() != null){
				contactNameTextView.setVisibility(View.VISIBLE);
				contactNameTextView.setText(dialog.getContactName());
			}
			else if(contactNameTextView != null){
				contactNameTextView.setVisibility(View.GONE);
			}
			
		}
		
		return v;
	}
}
