package litvinenkonikita.vksms.ui.list;

public class SmsDialogItem {
	
	private String mContactName;
	private String mNumber;
	private String mMessage;
	private String mDate;
	
	public SmsDialogItem(String contactName, String number, String message, String date){
		mContactName = contactName;
		mNumber = number;
		mMessage = message;
		mDate = date;
	}
	
	public String getContactName(){
		return mContactName;
	}
	
	public String getNumber(){
		return mNumber;
	}
	
	public String getMessage(){
		return mMessage;
	}
	
	public String getDate(){
		return mDate;
	}
	
}
