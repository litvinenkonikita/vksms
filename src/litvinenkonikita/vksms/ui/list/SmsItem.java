package litvinenkonikita.vksms.ui.list;

public class SmsItem {
	private String mDateTime;
	private String mText;
	private int mType;
	
	public SmsItem(String dateTime, String text, int type){
		mDateTime = dateTime;
		mText = text;
		mType = type;
	}
	
	public String getDateTime(){
		return mDateTime;
	}
	
	public String getText(){
		return mText;
	}
	
	public int getType(){
		return mType;
	}
	
}
