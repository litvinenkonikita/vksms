package litvinenkonikita.vksms.ui.list;

import java.util.List;

import litvinenkonikita.vksms.Const;
import litvinenkonikita.vksms.R;
import litvinenkonikita.vksms.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SmsListAdapter extends ArrayAdapter<SmsItem> {

	private static final String TAG = SmsListAdapter.class.getSimpleName();
	
	private int mResource;
	private Context mContext;
	
	public SmsListAdapter(Context context, int resource, List<SmsItem> objects){
		super(context, resource, objects);
		mContext = context;
		mResource = resource;
	}
	
	@Override 
	public View getView(int position, View convertView, ViewGroup parent){
		View v = convertView;
		
		SmsItem sms = getItem(position);
		
		if(v == null && sms != null){
			LayoutInflater vi = LayoutInflater.from(getContext());
			
			if(sms.getType() == Const.INCOME_SMS){
				v = vi.inflate(mResource, null);
			}
			else if(sms.getType() == Const.OUTCOME_SMS){
				v = vi.inflate(R.layout.out_msg_list_item, null);
			}
		}

		if(sms != null && v != null){
			///Utils.Log.d(TAG, "v = " + v);
			TextView smsTimeTextView = (TextView) v.findViewById(R.id.dateTime);
			TextView smsTextView = (TextView) v.findViewById(R.id.smsText);
			TextView smsTypeTextView = (TextView) v.findViewById(R.id.smsType);
			
			if(smsTimeTextView != null){
				smsTimeTextView.setText(sms.getDateTime());
			}
			
			if(smsTextView != null){
				smsTextView.setText(sms.getText());
			}
			
			if(smsTypeTextView != null){
				smsTypeTextView.setText(Utils.getSmsPrettyType(mContext, sms.getType()));
			}
		}
		
		return v;
	}
	
}
