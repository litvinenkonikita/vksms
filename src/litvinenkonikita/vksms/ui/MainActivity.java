package litvinenkonikita.vksms.ui;

import java.util.ArrayList;
import java.util.Map;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.vk.sdk.VKUIHelper;

import litvinenkonikita.vksms.Const;
import litvinenkonikita.vksms.R;
import litvinenkonikita.vksms.Utils;
import litvinenkonikita.vksms.sms.SMSManager;
import litvinenkonikita.vksms.ui.list.DialogsListAdapter;
import litvinenkonikita.vksms.ui.list.SmsDialogItem;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MainActivity extends ListActivity {
	
	private static final String TAG = MainActivity.class.getSimpleName();
	private static MainActivity sInstance;
	private ListView mListView;
	///private ArrayList<Map<String, Object>> mDialogsList;
	private ArrayList<SmsDialogItem> mDialogsList;
	private SMSManager mSMSManager;
	private SlidingMenu mSlidingMenu;
	
	public static MainActivity getInstance(){
		return sInstance;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mListView = getListView();
		setContentView(R.layout.activity_main);
		sInstance = this;
		mSMSManager = new SMSManager(this);
		setSmsDialogsList();
		mSlidingMenu = new SlidingMenu(this);
		///View headerView = findViewById(R.layout.header);
		///mListView.addHeaderView(headerView);
		
		mSlidingMenu.setMode(SlidingMenu.LEFT);
        mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mSlidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        mSlidingMenu.setShadowDrawable(R.drawable.shadow);
        mSlidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        mSlidingMenu.setFadeDegree(0.35f);
        ///mSlidingMenu.setAboveOffset(R.dimen.slidingmenu_offset);
        mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        ///mSlidingMenu.setMenu(R.layout.menu);
	}

	@Override
	protected void onResume(){
		super.onResume();
		VKUIHelper.onResume(this); 
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		VKUIHelper.onDestroy(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		VKUIHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		Intent smsActivityIntent = new Intent(this, SmsDialogActivity.class);
		///Map<String, Object> listItem = mDialogsList.get(position);
		SmsDialogItem listItem = mDialogsList.get(position);
		///smsActivityIntent.putExtra(Const.SMS_ADDRESS, (String)listItem.get(Const.SMS_ADDRESS));
		smsActivityIntent.putExtra(Const.SMS_ADDRESS, listItem.getNumber());
		smsActivityIntent.putExtra(Const.SMS_PERSON, listItem.getContactName());
		startActivity(smsActivityIntent);
	}
	/**
	private void setSmsDialogsList(){
		mDialogsList = mSMSManager.getSmsThreads();
		String[] from = { Const.SMS_ADDRESS, Const.SMS_BODY, Const.SMS_DATE, Const.SMS_PERSON };
		int[] to = { R.id.contactPhone, R.id.messageText, R.id.smsTime, R.id.contactName };
		SimpleAdapter adapter = new SimpleAdapter(this, mDialogsList, R.layout.dialog_list_item, from, to);
		setListAdapter(adapter);
	}*/
	
	private void setSmsDialogsList(){
		mDialogsList = mSMSManager.getSmsDialogs();
		DialogsListAdapter adapter = new DialogsListAdapter(this, R.layout.dialog_list_item, mDialogsList);
		setListAdapter(adapter);
	}

}
