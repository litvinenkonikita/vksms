package litvinenkonikita.vksms.ui;

import java.util.ArrayList;

import litvinenkonikita.vksms.Const;
import litvinenkonikita.vksms.R;
import litvinenkonikita.vksms.sms.SMSManager;
import litvinenkonikita.vksms.ui.list.DialogsListAdapter;
import litvinenkonikita.vksms.ui.list.SmsDialogItem;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class SmsDialogsListFragment extends ListFragment {
	
	private static final String TAG = SmsDialogsListFragment.class.getSimpleName();
	
	private ArrayList<SmsDialogItem> mDialogsList;
	private SMSManager mSMSManager;
	private FragmentActivity mContext;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mContext = getActivity();
		mSMSManager = new SMSManager(mContext);
		setSmsDialogsList();
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		Intent smsActivityIntent = new Intent(mContext, SmsDialogActivity.class);
		SmsDialogItem listItem = mDialogsList.get(position);
		///smsActivityIntent.putExtra(Const.SMS_ADDRESS, (String)listItem.get(Const.SMS_ADDRESS));
		smsActivityIntent.putExtra(Const.SMS_ADDRESS, listItem.getNumber());
		smsActivityIntent.putExtra(Const.SMS_PERSON, listItem.getContactName());
		startActivity(smsActivityIntent);
	}
	
	private void setSmsDialogsList(){
		mDialogsList = mSMSManager.getSmsDialogs();
		if(mDialogsList != null && mDialogsList.size() > 0){
			DialogsListAdapter adapter = new DialogsListAdapter(mContext, R.layout.dialog_list_item, mDialogsList);
			setListAdapter(adapter);
		}
		else{
			mContext.findViewById(R.id.no_sms_textview).setVisibility(View.VISIBLE);
			mContext.findViewById(R.id.dialogs_list_header).setVisibility(View.GONE);
		}
	}
}
