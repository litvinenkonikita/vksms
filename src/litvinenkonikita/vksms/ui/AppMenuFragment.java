package litvinenkonikita.vksms.ui;

import litvinenkonikita.vksms.R;
import litvinenkonikita.vksms.Utils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AppMenuFragment extends ListFragment {
	
	private static final String TAG = AppMenuFragment.class.getSimpleName();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.app_menu_fragment, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		String[] menuItems = getResources().getStringArray(R.array.menu_items);
		ArrayAdapter<String> itemsAdapter = 
				new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, menuItems);
		setListAdapter(itemsAdapter);
	}
	
	@Override
	public void onListItemClick(ListView lv, View v, int position, long id){
		Fragment newContent = null;
		switch(position){
			case 0:
				newContent = new SmsDialogsListFragment();
				break;
				
			case 1:
				newContent = null;
				break;
				
			case 2:
				newContent = new HelpFragment();
				break;
				
			default:
		}
		if (newContent != null)
			switchFragment(newContent);
	}
	
	private void switchFragment(Fragment fragment){
		FragmentActivity fragmentActivity = getActivity();
		if(fragmentActivity == null){
			return;
		}
		
		if(fragmentActivity instanceof FragmentChangeActivity){
			FragmentChangeActivity fca = (FragmentChangeActivity) fragmentActivity;
			fca.switchContent(fragment);
		}
	}
}
