package litvinenkonikita.vksms.ui;

import java.util.ArrayList;
import java.util.Map;

import com.vk.sdk.VKUIHelper;

import litvinenkonikita.vksms.Const;
import litvinenkonikita.vksms.R;
import litvinenkonikita.vksms.Utils;
import litvinenkonikita.vksms.sms.SMSManager;
import litvinenkonikita.vksms.ui.list.DialogsListAdapter;
import litvinenkonikita.vksms.ui.list.SmsItem;
import litvinenkonikita.vksms.ui.list.SmsListAdapter;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;

public class SmsDialogActivity extends ListActivity {
	
	private static final String TAG = SmsDialogActivity.class.getSimpleName();
	
	private static SmsDialogActivity sInstance;
	private ArrayList<SmsItem> mSmsList;
	private String mAddress;
	private String mContactName;
	private SMSManager mSMSManager;
	
	public SmsDialogActivity getInstance(){
		return sInstance;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sms);
		sInstance = this;
		Intent intent = getIntent();
		mAddress = intent.getStringExtra(Const.SMS_ADDRESS);
		mContactName = intent.getStringExtra(Const.SMS_PERSON);
		String namePhone;
		if(mContactName != null){
			namePhone = mContactName + " " + mAddress;
		}
		else{
			namePhone = mAddress;
		}
		this.setTitle(namePhone);
		mSMSManager = new SMSManager(this);
		setSmsList();
	}
	

	@Override
	protected void onResume(){
		super.onResume();
		VKUIHelper.onResume(this); 
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		VKUIHelper.onDestroy(this);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		VKUIHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	private void setSmsList(){
		mSmsList = mSMSManager.getSmsByAddress(mAddress);
		SmsListAdapter adapter = new SmsListAdapter(this, R.layout.in_msg_list_item, mSmsList);
		setListAdapter(adapter);
	}

}
