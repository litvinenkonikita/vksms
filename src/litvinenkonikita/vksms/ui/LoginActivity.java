package litvinenkonikita.vksms.ui;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCaptchaDialog;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKError;
import com.vk.sdk.util.VKUtil;

import litvinenkonikita.vksms.Const;
import litvinenkonikita.vksms.R;
import litvinenkonikita.vksms.R.menu;
import litvinenkonikita.vksms.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class LoginActivity extends Activity {
	
	private static final String TAG = LoginActivity.class.getSimpleName();
	private static final String[] sVkScope = new String[]{ VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS, VKScope.NOHTTPS };
	
	private VKSdkListener sdkListener = new VKSdkListener(){

		@Override
		public void onAccessDenied(VKError authorizationError) {
			new AlertDialog.Builder(LoginActivity.this).setMessage(authorizationError.errorMessage).show();
		}

		@Override
		public void onCaptchaError(VKError captchaError) {
			new VKCaptchaDialog(captchaError).show();
		}

		@Override
		public void onTokenExpired(VKAccessToken expiredToken) {
			VKSdk.authorize(sVkScope);
		}
		
		@Override
		public void onReceiveNewToken(VKAccessToken newToken){
			newToken.saveTokenToSharedPreferences(LoginActivity.this, Const.VK_TOKEN_KEY);
			///Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			Intent intent = new Intent(LoginActivity.this, FragmentChangeActivity.class);
			startActivity(intent);
		}
		
		@Override
		public void onAcceptUserToken(VKAccessToken newToken){
			///Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			Intent intent = new Intent(LoginActivity.this, FragmentChangeActivity.class);
			startActivity(intent);
		}
		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		VKSdk.initialize(sdkListener, Const.VK_APP_ID, VKAccessToken.tokenFromSharedPreferences(this, Const.VK_TOKEN_KEY));
		setContentView(R.layout.activity_login);
		
		findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				VKSdk.authorize(sVkScope, true, false);
			}
		});
		/**
		findViewById(R.id.force_oauth_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				VKSdk.authorize(sVkScope, true, true);
			}
		});
		*/
		
		///String[] fingerprint = VKUtil.getCertificateFingerprint(this, this.getPackageName());
		///Log.d(TAG, "Fingerprint: " + fingerprint[0]);
	}
	
	/**
	@Override
	protected void onStart(){
		super.onStart();
		
	}*/
	
	@Override
	protected void onResume(){
		super.onResume();
		VKUIHelper.onResume(this);
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		VKUIHelper.onDestroy(this);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		VKUIHelper.onActivityResult(requestCode, resultCode, data);
	}

}
