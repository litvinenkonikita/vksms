package litvinenkonikita.vksms.ui;

import litvinenkonikita.vksms.R;
import litvinenkonikita.vksms.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;

import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.vk.sdk.VKUIHelper;

public class FragmentChangeActivity extends SlidingFragmentActivity {
	
	private static final String TAG = FragmentChangeActivity.class.getSimpleName();
	private static FragmentChangeActivity sInstance;
	private Fragment mContent;
	private SlidingMenu mSlidingMenu;
	
	public static FragmentChangeActivity getInstance(){
		return sInstance;
	}

	private void actionBarInit() {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		setSlidingActionBarEnabled(false);
	}
	
	private void customizeSlidingMenu(){
		mSlidingMenu = getSlidingMenu();
		mSlidingMenu.setMode(SlidingMenu.LEFT);
        mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mSlidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        mSlidingMenu.setShadowDrawable(R.drawable.shadow);
        mSlidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        mSlidingMenu.setFadeDegree(0.35f);
        ///mSlidingMenu.setAboveOffset(R.dimen.slidingmenu_offset);
        ///mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        ///mSlidingMenu.setMenu(R.menu.main);
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		///setTitle(mTitleRes);
		sInstance = this;
		actionBarInit();
		if(savedInstanceState != null){
			mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
		}
		if(mContent == null){
			mContent = new SmsDialogsListFragment();
		}
		
		setContentView(R.layout.content_frame);
		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, mContent).commit();
		
		setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new AppMenuFragment()).commit();
		
		customizeSlidingMenu();
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		VKUIHelper.onResume(this); 
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		VKUIHelper.onDestroy(this);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		getSupportFragmentManager().putFragment(outState, "mContent", mContent);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		VKUIHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				toggle();
				return true;

		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    public void onBackPressed() {
        if (mSlidingMenu.isMenuShowing()) {
        	mSlidingMenu.toggle();
        }
        else {
            super.onBackPressed();
        }
    }
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            mSlidingMenu.toggle();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
	
	public void switchContent(Fragment fragment){
		mContent = fragment;
		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
		getSlidingMenu().showContent();
	}
}
