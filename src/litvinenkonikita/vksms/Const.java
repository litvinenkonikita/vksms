package litvinenkonikita.vksms;

public class Const {
	public static final boolean PRINT_LOG = true;
	
	public static final String VK_APP_ID = "4176101";
	public static final String VK_TOKEN_KEY = "VK_ACCESS_TOKEN";
	
	public static final String SMS_ID = "_id";
	public static final String SMS_THREAD_ID = "thread_id";
	public static final String SMS_ADDRESS = "address";
	public static final String SMS_PERSON = "person";
	public static final String SMS_DATE = "date";
	public static final String SMS_BODY = "body";
	public static final String SMS_TYPE = "type";
	
	public static final int INCOME_SMS = 1;
	public static final int OUTCOME_SMS = 2;
}
