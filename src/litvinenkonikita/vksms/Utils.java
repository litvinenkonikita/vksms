package litvinenkonikita.vksms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {
	
	public static void Log(String tag, Exception err){
		Log(tag, err, false);
	}
	
   public static void Log(String tag, Exception err, boolean notCritical){
	   if(Const.PRINT_LOG){
		   if(notCritical){
			   Utils.Log.d(tag, err.toString());
		   }
		   else{
			   Utils.Log.e(tag, err.toString());
		   }
		   
		   for(StackTraceElement trEl: err.getStackTrace()){
			   if(notCritical){
				   Utils.Log.d(tag, trEl.toString());
			   }
			   else{
				   Utils.Log.e(tag, trEl.toString());
			   }
		   }
		   
		   if(notCritical){
			   Utils.Log.d(tag, "Cause:");
		   }
		   else{
			   Utils.Log.e(tag, "Cause:");
		   }
		   
		   if (err.getCause() != null){
			   for ( StackTraceElement trEl: err.getCause().getStackTrace()){
				   if(notCritical){
					   Utils.Log.d(tag, trEl.toString());
				   }
				   else{
					   Utils.Log.e(tag, trEl.toString());
				   }
			   }
		   }
	   }
   }
	
	public static class Log{
		public static void v(String tag, String msg){
			if(Const.PRINT_LOG){
				android.util.Log.v(tag, msg);
			}
		}
   
		public static void w(String tag, String msg){
			if(Const.PRINT_LOG){
				android.util.Log.w(tag, msg);
			}
		}
   
		public static void i(String tag, String msg){
			if(Const.PRINT_LOG){
				android.util.Log.i(tag, msg);
			}
		}
   
		public static void e(String tag, String msg){
			if(Const.PRINT_LOG){
				android.util.Log.e(tag, msg);
			}
		}
   
		public static void d(String tag, String msg){
			if(Const.PRINT_LOG){
				android.util.Log.d(tag, msg);
			}
		}
	}
	
	
	public static boolean isOnline(Context context){
    	ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netInfo = connManager.getActiveNetworkInfo();
    	if(netInfo != null && netInfo.isConnected()){
    		return true;
    	}
    	return false;
    }
	
	
	public static String getSmsPrettyType(Context context, int type){
		if(type == Const.INCOME_SMS){
			return context.getString(R.string.income_sms);
		}
		else if(type == Const.OUTCOME_SMS){
			return context.getString(R.string.outcome_sms);
		}
		return null;
	}
}
